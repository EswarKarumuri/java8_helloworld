package com.example.java8sample;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Java8Tester {

	public static void main(String[] args) {

		List<String> list = new ArrayList<String>();
		list.add("Bujji");
		list.add("Bujjamma");
		System.out.println("List without sort: "+list);
		
		Java8Tester java8Tester = new Java8Tester();
		/*java8Tester.sortUsingJava7(list);
		System.out.println("Sorted using java7: "+ list);
		*/
		java8Tester.sortUsingJava8(list);
		System.out.println("Sorted using java8: "+ list);
		
		
	}
	
	public void sortUsingJava7(List<String> list){
		Collections.sort(list, new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return o1.compareTo(o2);
			}
		});
	}
	
	public void sortUsingJava8(List<String> list){
		Collections.sort(list, (s1, s2)->s1.compareTo(s2));
	}

}
